document.addEventListener('DOMContentLoaded', function() {
  const annonces = [
    { id: 1, marque: "Yamaha", modele: "YZF-R1", prix: "15000", image: "Annonces/GP235nf.jpg" },
    { id: 2, marque: "Honda", modele: "CBR600RR", prix: "12000", image: "Annonces/GR228NP.jpg" },
    { id: 3, marque: "BMW", modele: "S1000XR", prix: "12000", image: "Annonces/GR282SN.jpg" }
  ];

  const annoncesContainer = document.getElementById("latest-bikes");
  if (annoncesContainer) {
    annonces.forEach(annonce => {
      const annonceElement = document.createElement("div");
      annonceElement.className = "card";
      annonceElement.innerHTML = `
        <img src="${annonce.image}" alt="${annonce.modele}" class="image-moto" />
        <h4>${annonce.marque} ${annonce.modele}</h4>
        <p>${annonce.prix}€</p>
      `;
      annoncesContainer.appendChild(annonceElement);
    });
  }
});


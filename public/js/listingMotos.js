// js/listingMotos.js
import annonces from './motos.js';

let currentIndex = 0;
const itemsPerPage = 10;  // Définissez combien d'items vous voulez charger à la fois

function displayMotos() {
  const annoncesContainer = document.getElementById("moto-listing");
  const toLoad = annonces.slice(currentIndex, currentIndex + itemsPerPage);
  toLoad.forEach(annonce => {
    const annonceElement = document.createElement("div");
    annonceElement.className = "card";
    annonceElement.innerHTML = `
      <img src="${annonce.image}" alt="${annonce.modele}" class="image-moto" />
      <h4>${annonce.marque} ${annonce.modele}</h4>
      <p>${annonce.prix}€</p>
    `;
    annoncesContainer.appendChild(annonceElement);
  });
  currentIndex += itemsPerPage;
  if (currentIndex >= annonces.length) {
    document.getElementById("load-more").style.display = "none"; // Cachez le bouton s'il n'y a plus de données à charger
  }
}

document.addEventListener('DOMContentLoaded', function() {
  displayMotos(); // Affiche les premiers éléments

  document.getElementById('load-more').addEventListener('click', () => {
    displayMotos(); // Charge plus d'éléments lorsque l'utilisateur clique sur "Plus de résultats"
  });
});
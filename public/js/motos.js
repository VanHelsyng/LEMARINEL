// Dans js/motos.js
const annonces = [
  { id: 1, marque: "Yamaha", modele: "YZF-R1", prix: "15000", image: "Annonces/GP235nf.jpg" },
  { id: 2, marque: "Honda", modele: "CBR600RR", prix: "12000", image: "Annonces/GR228NP.jpg" },
  { id: 3, marque: "BMW", modele: "S1000XR", prix: "12000", image: "Annonces/GR282SN.jpg" },
  { id: 4, marque: "BMW", modele: "S1000XR", prix: "12000", image: "Annonces/GR282SN.jpg" },
  { id: 5, marque: "BMW", modele: "S1000XR", prix: "12000", image: "Annonces/GR282SN.jpg" },
  { id: 6, marque: "BMW", modele: "S1000XR", prix: "12000", image: "Annonces/GR282SN.jpg" },
  { id: 7, marque: "BMW", modele: "S1000XR", prix: "12000", image: "Annonces/GR282SN.jpg" },
  { id: 8, marque: "BMW", modele: "S1000XR", prix: "12000", image: "Annonces/GR282SN.jpg" },
  { id: 9, marque: "BMW", modele: "S1000XR", prix: "12000", image: "Annonces/GR282SN.jpg" },
  { id: 10, marque: "BMW", modele: "S1000XR", prix: "12000", image: "Annonces/GR282SN.jpg" },
  { id: 11, marque: "BMW", modele: "S1000XR", prix: "12000", image: "Annonces/GR282SN.jpg" },
  { id: 12, marque: "BMW", modele: "S1000XR", prix: "12000", image: "Annonces/GR282SN.jpg" },
  { id: 13, marque: "BMW", modele: "S1000XR", prix: "12000", image: "Annonces/GR282SN.jpg" }
];

export default annonces;
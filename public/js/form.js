document.getElementById('submit-bike').addEventListener('submit', function(event) {
  event.preventDefault(); // Empêcher la soumission standard du formulaire

  // Récupération et validation des champs du formulaire ici
  const modele = document.getElementById('bike-model').value;
  if (!modele) {
    alert('Le modèle de la moto est requis.');
    return;
  }

  // Si tout est valide, affichez un message ou effectuez une action
  alert('Annonce soumise avec succès!');
});
<?php
if($_SERVER["REQUEST_METHOD"] == "POST") {
    $marque = htmlspecialchars($_POST['marque']);
    $modele = htmlspecialchars($_POST['modele']);
    $prix = htmlspecialchars($_POST['prix']);
    $email = htmlspecialchars($_POST['email']);

    $to = 'mateolemarinel@free.fr'; // email
    $subject = 'Nouvelle annonce de moto';
    $message = "Vous avez reçu une nouvelle annonce pour une moto:\r\nMarque: $marque\r\nModèle: $modele\r\nPrix: $prix";
    $headers = 'From: webmaster@example.com' . "\r\n" .
               'Reply-To: ' . $email . "\r\n" .
               'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $message, $headers);

    echo 'Merci! Votre annonce a été soumise.';
} else {
    echo 'Accès non autorisé';
}
?>